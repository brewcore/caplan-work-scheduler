import path from 'path'
import express, { Request, Response, NextFunction } from 'express'
import { engine } from 'express-handlebars'
import { URL } from 'node:url'
import Schedule, { ScheduleConfig } from './lib/Schedule'
import { DateTime, Duration, Interval } from 'luxon'
import { mapAvailabilityBlocks } from './lib/IndividualScheduleViewMapper'

const __dirname = path.dirname(new URL(import.meta.url).pathname)
const app = express()
const port = 3333

app.engine('handlebars', engine())
app.set('view engine', 'handlebars')
app.set('views', path.join(__dirname, 'views'))

const scheduleInterval = Interval.fromDateTimes(DateTime.fromISO('2022-01-16T00:00:00.000-05:00'), DateTime.fromISO('2022-01-22T23:59:58.999-05:00'))

const scheduleConfig: ScheduleConfig = {
  start: scheduleInterval.start,
  end: scheduleInterval.end,
  workingHoursPerDay: Duration.fromObject({hours: 7.5}),
  nonWorkingDaysOfWeek: [6, 7],
  holidays: [DateTime.fromISO('2022-01-17')]
}

app.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const schedule = new Schedule([], scheduleConfig)
    const availabilities = schedule.getAvailabilityForDays(scheduleInterval)
    const loadBlocks = mapAvailabilityBlocks(availabilities)
    res.render('index', { loadBlocks, layout: false })
  } catch (err) {
    console.error(err)
  }
})

app.listen(port, () => {
  console.log(`Running on port ${port}`)
})
