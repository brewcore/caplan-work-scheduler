# Caplan Work Scheduler

This will be a tool for scheduling and capacity planning in ways that allow blending small discrete tasks with bigger vague initiatives.

The first goal is to plan and implement a proof-of-concept application to show the potential of this system and work out some UI concepts.

## Mockups

### Individual Schedule
- [View Interactive Mockup](https://brewcore.gitlab.io/caplan-work-scheduler/html-mocks/individual-schedule-mock.html)

![Individual Schedule Preview](/htmlMocks/individual-schedule-mock.png "Individual Schedule Preview")

## Copyright

Copyright (C) 2021 Tyler Golden

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Affero General Public License](LICENSE) for more details.
