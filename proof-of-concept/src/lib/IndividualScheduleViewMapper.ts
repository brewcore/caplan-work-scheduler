/*
This file is part of the Caplan software tool.

Copyright (C) 2021 Tyler Golden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import { AvailabilityForDay } from "./Schedule"

export interface IndividualScheduleBlockDisplay {
  groupName: string
  blockTitle: string
  style: string
  tooltip: string
  startDate: Date
  endDate: Date
}

export function mapAvailabilityBlocks(availabilities: AvailabilityForDay[]): IndividualScheduleBlockDisplay[] {
  return availabilities.map(availability => {
    const hoursText = `${availability.capacityHours.toString()}h`
    const style = availability.capacityHours > 0 ? 'color: #66aa66;' : ''
    return {
      groupName: 'Availability',
      blockTitle: hoursText,
      style,
      tooltip: hoursText,
      startDate: availability.date.toJSDate(),
      endDate: availability.date.plus({days: 1}).toJSDate()
    }
  })
}
