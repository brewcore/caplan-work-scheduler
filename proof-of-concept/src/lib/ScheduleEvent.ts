/*
This file is part of the Caplan software tool.

Copyright (C) 2021 Tyler Golden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import { DateTime } from 'luxon'

export default class ScheduleEvent {
  private readonly _start: DateTime
  private readonly _end: DateTime

  constructor (start: DateTime, end: DateTime) {
    this._start = start
    this._end = end
  }

  get Start() {
    return this._start
  }

  get End() {
    return this._end
  }
}